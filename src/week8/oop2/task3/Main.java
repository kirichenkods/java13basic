package week8.oop2.task3;

public class Main {
    public static void main(String[] args) {
        SimpleArrayList mySimpleArrayList = new SimpleArrayList();
        mySimpleArrayList.add(12);
        mySimpleArrayList.add(13);
        mySimpleArrayList.add(15);
        mySimpleArrayList.add(16);
        mySimpleArrayList.add(17);
        mySimpleArrayList.add(18);
        System.out.println("ArrayList size: " + mySimpleArrayList.size());
        System.out.println("Элемент с первым индексом: " + mySimpleArrayList.get(0));
        System.out.println("Элемент с последним индексом: " + mySimpleArrayList.get(mySimpleArrayList.size()));
    }
}
