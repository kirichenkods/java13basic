package week7.oop1.task2;

public class Main {
    public static void main(String[] args) {
        Thermometer thermometer = new Thermometer(-25, TemperatureUnit.CELSIUS);
        Thermometer thermometer2 = new Thermometer(-25, TemperatureUnit.FAHRENHEIT);
        System.out.println("В Цельсиях: " + thermometer.getTempCelsius());
        System.out.println("В Фаренгейтах: " + thermometer.getTempFahrenheit());
        System.out.println("В Цельсиях: " + thermometer2.getTempCelsius());
        System.out.println("В Фаренгейтах: " + thermometer2.getTempFahrenheit());
    }
}
