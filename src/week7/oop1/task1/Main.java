package week7.oop1.task1;

public class Main {
    public static void main(String[] args) {
        Bulb bulb = new Bulb();
//        Bulb bulb1 = new Bulb(false);
        System.out.println("Светит ли лампа сейчас? - " + bulb.isShining());
        bulb.turnOn();
        System.out.println("Светит ли лампа сейчас? - " + bulb.isShining());

        Chandelier chandelier = new Chandelier(4);
        chandelier.turnOn();
        chandelier.turnOff();
    }
}
