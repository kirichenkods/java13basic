package fourthweekpractice;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int countNeg = (n > 0) ? 0 : 1;
        while (n < 0) {
            n = scanner.nextInt();
            if (n < 0) {
                countNeg++;
            }
        }

        System.out.println(countNeg);
    }
}
