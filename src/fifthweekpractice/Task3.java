package fifthweekpractice;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }

        boolean result = true;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] <= arr[i + 1]) {
                result = false;
                break;
            }
        }

        System.out.println(result);
        System.out.println(checkIfArrayDesc(arr));
    }

    public static boolean checkIfArrayDesc(int[] inputArray) {
        for (int i = 0; i < inputArray.length - 1; i++) {
            if (inputArray[i] <= inputArray[i + 1]) {
                return false;
            }
        }
        return true;
    }
}
