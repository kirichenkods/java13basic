package fourthweekpractice;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int p = scanner.nextInt();
        double start = 1000;
        double end = 1100;
        int month = 0;
        while (start < end) {
            start += start * p / 100.0;
            month++;
        }
        System.out.println("Вклад: " + start + " количество месяцев: " + month);

    }
}
