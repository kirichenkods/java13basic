package fifthweekpractice;

import java.util.Arrays;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr1 = new int[n];
        for (int i = 0; i < arr1.length; i++) {
            arr1[i] = scanner.nextInt();
        }

        int k = scanner.nextInt();
        int[] arr2 = new int[k];
        for (int i = 0; i < arr2.length; i++) {
            arr2[i] = scanner.nextInt();
        }

        mergeTwoArraysWithLoop(arr1, arr2);
        mergeTwoArraysWithSystemArrayCopy(arr1, arr2);
        mergeTwoArrays(arr1, arr2);


    }

    /**
     * метод делает слияние двух массивов в третий результирующий и сортирует его
     * @param arr1
     * @param arr2
     */
    public static void mergeTwoArraysWithLoop(int[] arr1, int[] arr2) {
        int[] mergeArray = new int[arr1.length + arr2.length];

        int pos = 0;
        for (int element : arr1) {
            mergeArray[pos] = element;
            pos++;
        }

        for (int element : arr2) {
            mergeArray[pos] = element;
            pos++;
        }

        Arrays.sort(mergeArray);
        System.out.println(Arrays.toString(mergeArray));
    }

    public static void mergeTwoArraysWithSystemArrayCopy(int[] arr1, int[] arr2) {
        int[] mergedArray = new int[arr1.length + arr2.length];

        int pos = 0;
        System.arraycopy(arr1, 0, mergedArray, 0, arr1.length);
        System.arraycopy(arr2, 0, mergedArray, arr1.length, arr2.length);
        Arrays.sort(mergedArray);
        System.out.println(Arrays.toString(mergedArray));
    }

    public static void mergeTwoArrays(int[] arr1, int[]arr2) {
        int[] mergedArray = new int[arr1.length + arr2.length];
        int i = 0, j = 0, k = 0;

        while (i < arr1.length && j < arr2.length) {
            if (arr1[i] < arr2[j]) {
                mergedArray[k++] = arr1[i++];
            } else {
                mergedArray[k++] = arr2[j++];
            }
        }

        while (i < arr1.length) {
            mergedArray[k++] = arr1[i++];
        }

        while (j < arr2.length) {
            mergedArray[k++] = arr2[j++];
        }

        Arrays.sort(mergedArray);

        System.out.println(Arrays.toString(mergedArray));
    }
}
