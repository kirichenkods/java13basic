package secondweekpractice;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String a1 = scanner.next();
        String a2 = scanner.next();
        String a3 = scanner.next();
        String a4 = scanner.next();
        String a5 = scanner.next();

        String result = "";
        result += Character.isDigit(a1.charAt(0)) ? "" : a1;
        result += Character.isDigit(a2.charAt(0)) ? "" : a2;
        result += Character.isDigit(a3.charAt(0)) ? "" : a3;
        result += Character.isDigit(a4.charAt(0)) ? "" : a4;
        result += Character.isDigit(a5.charAt(0)) ? "" : a5;

        System.out.println(result);
    }
}
