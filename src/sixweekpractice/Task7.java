package sixweekpractice;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[][] a = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = scanner.nextInt();
            }
        }
        int p = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (a[i][j] == p) {
                    fillWithZero(a, n, i, j, p);
                }
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static void fillWithZero(int[][] a, int n, int iInd, int jInd, int p) {
        for (int k = 0; k < n; k++) {
            if (a[k][jInd] != p) {
                a[k][jInd] = 0;
            }

            if (a[iInd][k] != p) {
                a[iInd][k] = 0;
            }
        }
    }
}
