package week7.oop1.task1;
/*
Реализовать класс “Лампа”. Методы:
включить лампу
выключить лампу
получить текущее состояние
*/
public class Bulb {
    private boolean toggle;

    public Bulb() {
        this.toggle = false;
    }
//    public Bulb(boolean conditon) {
//        this.toggle = conditon;
//    }
    public void turnOn() {
        this.toggle = true;
    }

    public void turnOff() {
        this.toggle = false;
    }

    public boolean isShining() {
        return this.toggle;
    }
}
