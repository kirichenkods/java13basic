package fifthweekpractice;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr1 = new int[n];
        boolean flag = false;

        for (int i = 0; i < n; i++) {
            arr1[i] = scanner.nextInt();
        }

        int m = scanner.nextInt();

        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (arr1[i] + arr1[j] == m) {
                    System.out.println(arr1[i] + " " + arr1[j]);
                    flag = true;
                }
            }
        }

        if (!flag) {
            System.out.println(-1);
        }



    }
}
