package week8.oop2.task3;

import java.util.Arrays;

/*
Примитивная реализация ArrayList.
Массив только int, из методов только добавлять элемент,
получать size и увеличивать капасити, когда добавляется новый.
*/
public class SimpleArrayList {
    private int size;
    private int[] arr;
    private int capacity;
    private static final int DEFAULT_CAPACITY = 5;
    private int currIndex;

    public SimpleArrayList() {
        arr = new int[DEFAULT_CAPACITY];
        capacity = DEFAULT_CAPACITY;
        size = 0;
        currIndex = 0;
    }

    public SimpleArrayList(int size) {
        arr = new int[size];
        capacity = size;
        this.size = 0;
        currIndex = 0;
    }

    public void add(int element) {
        if (currIndex >= capacity) {
            capacity = 2 * capacity;
            arr = Arrays.copyOf(arr, capacity);
        }

        arr[currIndex] = element;
        size++;
        currIndex++;
    }

    public int get(int idx) {
        if (idx < 0 || idx >= size) {
            System.out.println("Невозможно взять элемент по заданному индексу: " + idx);
            return -1;
        } else {
            return arr[idx];
        }
    }

    public int size() {
        return size - 1;
    }

}
