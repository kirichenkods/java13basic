package firstweekpractice;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double s = scanner.nextDouble();
        double d = Math.sqrt(s * 4 / Math.PI);
        double l = Math.sqrt(s * 4 * Math.PI);
        System.out.println("Диаметр окружности: " + d + ", длина окружности: " + l);
    }
}
