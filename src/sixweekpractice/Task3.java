package sixweekpractice;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();

        System.out.println(reverseString(s));
    }

    public static String reverseString(String s) {
        if (s.length() == 1) {
            return s;
        }

        return s.substring(s.length() - 1) + reverseString(s.substring(0, s.length() - 1));

//        if (s.isEmpty()) {
//            return s;
//        } else {
//            return reverseString(s.substring(1)) + s.charAt(0);
//        }
    }
}
