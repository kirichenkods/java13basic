package secondweekpractice;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String str = "";
        if (n % 2 == 0 && n >= 0) {
            str = "Четное больше или равно 0";
        }
        if (n % 2 == 0 && n < 0) {
            str = "Четное меньше 0";
        }
        System.out.println(str);
    }
}
