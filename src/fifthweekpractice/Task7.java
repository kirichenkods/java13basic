package fifthweekpractice;

import java.util.Arrays;
import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[] arr = new String[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.next();
        }
        int m = scanner.nextInt();

        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].length() <= m) {
                count++;
            }
        }

        String[] arrRes = new String[count];
        int j = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].length() <= m) {
                arrRes[j++] = arr[i];
            }
        }

        System.out.println(Arrays.toString(arrRes));

    }
}
