package secondweekpractice;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        int sum;
        sum = a + b + c - Math.min(a, Math.min(b, c));
        System.out.println(sum);
    }
}
