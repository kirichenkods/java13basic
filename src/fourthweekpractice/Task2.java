package fourthweekpractice;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();

        int res = 1;
        for (int i = m; i <= n; i++) {
            res *= i;
        }

        System.out.println("Результат равен: " + res);
    }
}
