package week8.oop2.task1;

public class VariableLength {

    static int sum(int... numbers) {
        int sum = 0;

        for (int number : numbers) {
            sum += number;
        }

        return sum;
    }

    static boolean findChar(Character ch, String... strings) {
        for (String string : strings) {
            if (string.indexOf(ch) != -1) {
                return true;
            }
        }

        return false;
    }

    public static void main(String[] args) {
        System.out.println(sum(1, 2, 3, 5, 4, 6));
        System.out.println(findChar('a', "python", "java"));

        System.out.println(String.format("This is an integer: %d", 123));
        String test = """
                select * from table1;
                """;
    }
}
