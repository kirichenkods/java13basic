package week7.oop1.task3;

public class Main {
    public static void main(String[] args) {
        System.out.println(FieldValidator.validateEmail("andy@bk.ru"));
        System.out.println(FieldValidator.validateDate("12.12.2000"));
        System.out.println(FieldValidator.validatePhone("+79991112233"));
        System.out.println(FieldValidator.validateName("Boris"));
    }
}
