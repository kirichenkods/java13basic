package sixweekpractice;

import java.util.Arrays;
import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();

        int[][] a = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] = scanner.nextInt();
            }
        }

        int[] res = new int[m];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                res[i] += a[j][i];
            }
        }

        System.out.println(Arrays.toString(res));
    }
}
