package firstweekpractice;

import java.util.Scanner;

public class Task1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        int temp = c;
        c = b;
        b = a;
        a = temp;
        System.out.printf("a = %s, b = %s, c = %s", a, b, c);
    }
}
