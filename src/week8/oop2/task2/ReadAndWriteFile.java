package week8.oop2.task2;

import java.io.*;
import java.util.Scanner;

public class ReadAndWriteFile {
    private static final String FOLDER_DIRECTORY = "/home/omega/IdeaProjects/java13basic/src/week8/oop2/task2/file";
    private static final String OUTPUT_FILE_NAME = "output.txt";

    private ReadAndWriteFile() throws IOException {
    }

    public static void readAndWriteData(String filePath) throws IOException {
        Scanner scanner = new Scanner(new File(filePath));
        String[] days = new String[10];
        int i = 0;
        while (scanner.hasNextLine()) {
            days[i++] = scanner.nextLine();
        }

        Writer writer = new FileWriter(FOLDER_DIRECTORY + "/" + OUTPUT_FILE_NAME);
        for (int j = 0; j < i; j++) {
            String res = "Порядковый номер дня недели " + days[j] + " = " +
                    WeekDays.ofName(days[j]).dayNumber + "\n";
            writer.write(res);
        }

        writer.close();
        scanner.close();
    }

    public static void readAndWriteData() throws IOException {
        readAndWriteData(FOLDER_DIRECTORY + "/" + "input.txt");
    }
}
