package fourthweekpractice;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        int n = scanner.nextInt();
//        String num = String.valueOf(n);
//
//        int sum = 0;
//        for (int i = 0; i < num.length(); i++) {
//            sum += Integer.parseInt(String.valueOf(num.charAt(i)));
//        }
//        System.out.println(sum);


        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int sum = 0;
        while (n > 0) {
            sum = sum + n % 10;

            n = n / 10;
        }
        System.out.println(sum);
    }
}
