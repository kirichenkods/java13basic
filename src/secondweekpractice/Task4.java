package secondweekpractice;

import java.util.Scanner;

public class Task4 {
    public static final int VIP_PRICE = 125;
    public static final int PREMIUM_PRICE = 110;
    public static final int STANDART_PRICE = 100;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int typeRoom = scanner.nextInt();
        switch (typeRoom) {
            case (1) -> System.out.println("VIP: " + VIP_PRICE);
            case (2) -> System.out.println("Premium: " + PREMIUM_PRICE);
            case (3) -> System.out.println("Standart: " + STANDART_PRICE);
            default -> System.out.println("Введите корректный номер");
        }
    }
}
