package secondweekpractice;

import java.util.Calendar;
import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.next();
        char ch = str.charAt(0);
//        if (ch > 'A' && ch < 'Z') {
//            System.out.println(Character.toLowerCase(ch));
//        } else {
//            System.out.println(Character.toUpperCase(ch));
//        }
        if (ch >= 'a' && ch <= 'z') {
            System.out.println((char) (ch + ('A' - 'a')));
        } else {
            System.out.println((char) (ch - ('A' - 'a')));
        }
    }
}
