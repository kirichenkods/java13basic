package secondweekpractice;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        boolean result;

        result = ((a != 0 && b != 0) && a + b == 0);

        if (!result) {
            result = ((b != 0 && c != 0) && b + c == 0);
        }

        if (!result) {
            result = ((a != 0 && c != 0) && a + c == 0);
        }

        System.out.println(result);

    }
}
