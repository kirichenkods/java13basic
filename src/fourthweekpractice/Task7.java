package fourthweekpractice;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int firstNumber = scanner.nextInt();
        int secondNumber = scanner.nextInt();
        int temp = Math.max(firstNumber, secondNumber);
        firstNumber = Math.min(firstNumber, secondNumber);
        secondNumber = temp;

        for (int i = 2; i < n; i++) {
            int num = scanner.nextInt();
            if (num >= secondNumber) {
                firstNumber = secondNumber;
                secondNumber = num;
            }
            if (num >= firstNumber) {
                firstNumber = num;
            }
        }
        System.out.println(firstNumber + " " + secondNumber);
    }
}
