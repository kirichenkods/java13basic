package fifthweekpractice;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }

        boolean flag = false;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                flag = true;
                System.out.print(arr[i] + " ");
            }
        }
        if (!flag) {
            System.out.println(-1);
        }
    }
}
