package secondweekpractice;

import java.util.Locale;
import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        String pattern = scanner.next();
        System.out.println(str.replace(pattern, pattern.toUpperCase()));
    }
}
